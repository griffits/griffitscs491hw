﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnGUI()
    {
        if (GUI.Button(new Rect(30, 470, 200, 100), "Easy"))
        {
            Application.LoadLevel("Level1");
        }
        if (GUI.Button(new Rect(330, 470, 200, 100), "Medium"))
        {
            Application.LoadLevel("Level2");
        }
        if (GUI.Button(new Rect(630, 470, 200, 100), "Hard"))
        {
            Application.LoadLevel("Level3");
        }
    }
}
