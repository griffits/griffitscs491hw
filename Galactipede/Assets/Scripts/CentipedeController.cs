﻿using UnityEngine;
using System.Collections;

public class CentipedeController : MonoBehaviour {
    private Rigidbody2D body;
    public float speed;
    Vector3 playerPosition;
    // Use this for initialization
    void Start () {
        body = GetComponent<Rigidbody2D>();
	    body.velocity = new Vector3(speed, 0, 0);
        playerPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "RightWall" || collision.GetComponent<Collider2D>().gameObject.tag == "Asteroid" || collision.gameObject.name == "LeftWall")
        {
            print("hit");
            speed = -speed;
            transform.Translate(0f, -.5f, 0f);
            body.velocity = new Vector3(speed, 0, 0);
        }
        if (collision.GetComponent<Collider2D>().gameObject.tag == "Bullet")
        {
            Destroy(gameObject);
        }
        if (collision.GetComponent<Collider2D>().gameObject.tag == "Ship")
        {
            Application.LoadLevel("Loss Screen");
        }
    }
}
