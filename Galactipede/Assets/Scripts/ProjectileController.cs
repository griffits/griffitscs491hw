﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {
    public GameObject projectile;
    private Transform shotPos;
    private float nextShot = 0.0f;
    private float interval = 0.9f;
    
   
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown("space"))
        {
            if (Time.time >= nextShot)
            {
                nextShot = Time.time + interval;
                GameObject shot = (GameObject)Instantiate(projectile, transform.TransformPoint(Vector3.up * .4f), Quaternion.identity);
            }
        }
        
    }
  }
