﻿using UnityEngine;
using System.Collections;

public class PewPewScript : MonoBehaviour {
    public float shotForce = 5f;

    private Rigidbody2D body;
    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        body.AddForce(Vector3.up * shotForce);
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Ceiling" || collision.gameObject.name == "Asteroid")
        {
            print("hit");
            Destroy(gameObject);
        }
        if (collision.GetComponent<Collider2D>().gameObject.tag == "Centipede")
        {
            Destroy(gameObject);
        }

    }
}
