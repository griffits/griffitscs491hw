﻿using UnityEngine;
using System.Collections;

public class StartController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        GUI.Label(new Rect(550, 155, 300, 300), "WELCOME TO THE TIMED THEME TEST! GUESS THE THEME OF THE PICTURES GIVEN! YOU WILL HAVE 60 SECONDS FOR EACH LEVEL. FAILURE TO COMPLETE A LEVEL RESULTS IN A LOSS. YOU WILL RECIEVE HINTS EVERY 15 SECONDS! GOOD LUCK!");

        if (GUI.Button(new Rect(550, 285, 300, 150), "Start"))
        {
            Application.LoadLevel("Level1");
        }
     }
}
