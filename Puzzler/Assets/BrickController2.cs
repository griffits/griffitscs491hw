﻿using UnityEngine;
using System.Collections;

public class BrickController2 : MonoBehaviour {

    public Sprite frontSprite;
    public Sprite backSprite;
    private bool isVisible;

	// Use this for initialization
	void Start () {
        frontSprite = GetComponent<SpriteRenderer>().sprite;
        SetVisible(true);
	}
	
    public void SetVisible(bool isVisible)
    {
        this.isVisible = isVisible;
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (isVisible)
        {
            //sr.color = new Color(1, 1, 1);
            GetComponent<SpriteRenderer>().sprite = frontSprite;
        }
        else
        {
           // sr.color = new Color(0, 0, 0);
            GetComponent<SpriteRenderer>().sprite = backSprite;
        }
        sr.sprite = isVisible ? frontSprite: backSprite;

    }

    public void OnMouseUp()
    {
        StartCoroutine(GameController2.S.OnFlip(this));
        if (!isVisible)
        {
            StartCoroutine(GameController2.S.OnFlip(this));
                }
    }
}
