﻿using UnityEngine;
using System.Collections;

public class GameController1 : MonoBehaviour {
    public static GameController1 S;
    private GameObject[] bricks;
    public GameObject[] answers;
    public bool answer = false;
    private AudioSource audioSource;
    public AudioClip timerSounds;
    public float startTime;
    public float ellapsedTime;
    public int counter;
    public int score = 100;
    void Awake()
    {
        startTime = Time.time;
        S = this;
        audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {
        S = this;
        bricks = Resources.LoadAll<GameObject>("bricks");
        for (int i = 0; i < 3; i++)
      {
            GameObject temp = GameObject.Instantiate<GameObject>(bricks[i]);
          temp.transform.position = new Vector2(i * 3, 0);
     }

    }
        // Update is called once per frame
        void Update () {

        audioSource.loop = true;
        audioSource.PlayOneShot(timerSounds, 1.0f);

    }

    public BrickController1 brick1;
    public BrickController1 brick2;
    public BrickController1 brick3;
  


    public IEnumerator OnFlip(BrickController1 brick)
    {
        

        if (brick1 == null)
        {
            brick1 = brick;
            brick.SetVisible(true);
        }else if(brick2 == null)
        {
            brick2 = brick;
            brick.SetVisible(true);
        }
        else if (brick3 == null)
        {
            brick3 = brick;
            brick.SetVisible(true);
        }
       

        yield return new WaitForSeconds(1);


       
    }

    public string stringToEdit = "ANSWER";

    void OnGUI()
    {
        if (Time.time - startTime > 15)
        {
            if (GUI.Button(new Rect(10, 25, 100, 50), "HINT 1"))
            {
                score -= 10;
                counter++;
                answers = Resources.LoadAll<GameObject>("answers2");
                for (int i = 0; i < 1; i++)
                {
                    GameObject temp = GameObject.Instantiate<GameObject>(answers[i]);
                    temp.transform.position = new Vector2(i * 3, 0);
                }
                Debug.Log("CLICKED");
            }
        }
        if (Time.time - startTime > 30)
        {
            if (GUI.Button(new Rect(110, 25, 100, 50), "HINT 2"))
            {
                score -= 20;
                counter++;
                answers = Resources.LoadAll<GameObject>("answers2");
                for (int i = 1; i < 2; i++)
                {
                    GameObject temp = GameObject.Instantiate<GameObject>(answers[i]);
                    temp.transform.position = new Vector2(i * 3, 0);
                }
            }
        }
        if (Time.time - startTime > 45)
        {
            if (GUI.Button(new Rect(210, 25, 100, 50), "HINT 3"))
            {
                score -= 30;
                counter++;
                answers = Resources.LoadAll<GameObject>("answers2");
                for (int i = 2; i < 3; i++)
                {
                    GameObject temp = GameObject.Instantiate<GameObject>(answers[i]);
                    temp.transform.position = new Vector2(i * 3, 0);
                }
            }
        }
        stringToEdit = GUI.TextField(new Rect(500, 500, 300, 50), stringToEdit, 500);
        if (stringToEdit.ToLower().Equals("tall"))
        {
            answer = true;
            if (answer == true)
            {
                Application.LoadLevel("Level3");
            }
            Debug.Log("CORRECT!");
        }
        else
        {
            Debug.Log("WRONG");
        }

        
        if(Time.time - startTime > 60.0)
        {
            ellapsedTime = -1.0f;
            if (ellapsedTime == -1)
            {
                Application.LoadLevel("GameOver");
            }
        }
        else
        {
            ellapsedTime = Time.time - startTime;
        }
        GUI.Label(new Rect(800,25,100,50), ellapsedTime.ToString());

        if(ellapsedTime < 10.0)
        {
            ellapsedTime = 0.0f;
            
        }
    }

    public void OnMouseUp()
    {

        
    }
}
