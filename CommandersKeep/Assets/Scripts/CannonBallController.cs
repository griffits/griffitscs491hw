﻿using UnityEngine;
using System.Collections;

public class CannonBallController : MonoBehaviour {
	private Rigidbody2D body;
	public float speed;
	// Use this for initialization
	void Start () {
	 body = GetComponent<Rigidbody2D>();
	int ranNum  = Random.Range(0,1) *2 -1;
	print(ranNum);
	 body.velocity = new Vector2(speed * ranNum, 0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.collider.CompareTag("Wall") ) {
			speed = -speed;
			body.velocity = new Vector2(speed, 0f);
		}
	}
}
