﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {
	private Rigidbody2D body;
	private BoxCollider2D collider;
	private Vector2 enemyMovement;
	private int direction;
	public float enemySpeed;
	public LayerMask groundLayer;
	// Use this for initialization
	void Start () {
		collider = GetComponent<BoxCollider2D> ();
		body = GetComponent<Rigidbody2D> ();
		enemyMovement = new Vector2 (enemySpeed, 0);
		direction = 1;

	}
	
	// Update is called once per frame
	void Update () {

		if (isGrounded ()) {
			body.velocity = enemyMovement*direction;
		}

		//check for edge and change direction
		if (edgeDetected ()) {
			direction *= -1;
		}
	}

	bool isGrounded() {
		Vector3 topright = new Vector3 (collider.size.x / 2-.1f, collider.size.y / 2-.1f) + transform.position;
		Vector3 bottomleft = new Vector3(-collider.size.x / 2-.1f, -collider.size.y / 2-.1f) + transform.position;
		//print (topright + " " + bottomleft);
		return Physics2D.OverlapArea(topright, bottomleft, groundLayer);
	}

	bool edgeDetected() {
		bool ret = false;
		RaycastHit2D hit = Physics2D.Raycast (new Vector2 (direction*collider.size.x/2f + direction*0.1f + transform.position.x, transform.position.y), Vector2.down);
		if (hit.collider == null) {
			ret = true;
		}
		return ret;
	}
}

