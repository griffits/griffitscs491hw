﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	
	public float speed;
	private Rigidbody2D body;
	private float run;
	public float jumpForce;
	private Animator animator;
	private bool swinging;
	private bool isGrounded;
	
		
	void Start () {
		body = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
		swinging = false;
		isGrounded = true;
	}
	
	
	void Update () {
		
		float max = -6.2f;
		float min = -6.7f;
		
		if(Camera.main.transform.position.y > max){
			 Camera.main.transform.position = new Vector3(transform.position.x,
		  max, Camera.main.transform.position.z);
		}
		if(Camera.main.transform.position.y < min){
			 Camera.main.transform.position = new Vector3(transform.position.x,
		  min, Camera.main.transform.position.z);
		}
		if(Camera.main.transform.position.y <= max && Camera.main.transform.
		position.y >= min){ 
			 Camera.main.transform.position = new Vector3(transform.position.x,
		  Camera.main.transform.position.y, Camera.main.transform.position.z);
		}
		
		
		
		
		run = Input.GetAxis("Horizontal");
		if( body.velocity.y == 0){
			isGrounded = true;
		}
		body.velocity = new Vector3(run * speed, 0f, 0f);
		if ( Input.GetKeyDown(KeyCode.UpArrow) && isGrounded){
			print("jump");
			body.AddForce(new Vector2(0, jumpForce * 50f));
		}
		//Moving
		if (run != 0) { 
			animator.SetBool ("move", true);
			
		} else {
			animator.SetBool ("move", false);
		}
		// Falling
		if (body.velocity.y != 0 ) {
			animator.SetBool ("fall", true);
			
		} else {
			animator.SetBool ("fall", false);
		}
		// Swinging
		if (swinging) {
			animator.SetBool ("swing", true);
			
		} else {
			animator.SetBool("swing", false);
		}
		
	}
}
